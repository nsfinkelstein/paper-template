\documentclass[main.tex]{subfiles}

\begin{document}
\section{Introduction}

Large and increasing portions of human, economic, and medical transacations
either occur or are recorded digitally. Many of these records are of substantial
interest to scientists in a number of fields. However, unlike data that have
traditionally been used in the course of scientific inquiry, these data are not
created expressly to aid in that inquiry. They are instead created for an
entirely different set of purposes, which in turn shape and distort them. As a
result, many vitally important questions cannot be exactly answered through the
use of such data. We propose to study methods that can use observational data to
provide \textit{partial} answers to such questions.

This work is motivated in particular by electronic medical record (EMR) data.
Over the past ten years, federal policy has pushed the large majority of
major healthcare providers to digitize their records systems, leading to an
explosion in the prevelance and availability of EMRs. However, due both to
federal laws and to hospital incentives, these systems are primarily designed to
keep track of procedures for billing purposes. This means that any misleading
claims the hospital makes to insurance companies to encourage payment will
appear in the EMR data. In addition, EMR data suffers from many other problems
such as missing data, unobserved confounding, and so on. These issues are often
insurmountable obstacles to extracting exact answers to scientific questions
from these data. Nevertheless, EMRs present an incredibly rich and granular
image of patient trajectories during care. We hope that the ideas developed and
proposed below will help provide tools that enable principled use of this
tremendous resource to improve patient care.

In this thesis proposal, we are primarily interested in questions that involve
counterfactual reasoning. A canonical example of such a question is ``what is
the difference between what the average outcome would have been if everyone were
given treatment A, as contrasted with treatment B?'' Another such question is
``what is the probability that event E still would have happened, had an earlier
event F not happened?'' The numeric answers to such questions are called
counterfactual parameters, or targets of inference.

If exact answers to such questions are available, the corresponding targets of
inference are said to be \textit{exactly identified}. If instead they can be
bounded or otherwise restricted, they are said to be \textit{partially
  identified}. Exact identification methods are more readily available than, and
tend to be preferred to, partial identification methods. As a result, analysts
who encounter scenarios in which exact answers are not available under plausible
assumptions will often choose one of two avenues: (i) assume that because no
exact answer is available, the data contains no information at all about the
question, leaving potential scientific progress on the table, or (ii) make an
assumption that is not plausible in their setting for the sake of gaining access
to an exact identification method. Both avenues are undersireable. Our goal is
to provide partial identification methods that are widely and easily applicable
for the sake of expanding the set of principled approaches available to analysts
in such settings.

In this proposal, we discuss approaches to obtaining partial identification in a
number of scenarios. These approaches are in varying stages of development. The
proposed thesis will be comprised of methods based on the ideas
explored in the following sections. In \S \ref{sec:preliminaries} we
introduce relevant concepts and mathematical background. In \S
\ref{sec:measurement-error}, we make use of linear programming to
obtain bounds under measurement error. In \S \ref{sec:parametric-simplex}, we
develop a symbolic optimization procedure based on the simplex algorithm to
obtain symbolic bounds in the resulting linear programs. In \S
\ref{sec:causal-bounds}, we develop a suite of tools to provide bounds on causal
quantities in models that cannot be represented linearly. In \S
\ref{sec:missingness-bounds} we discuss partial identification results in 
missing data settings. In \S \ref{sec:explanation}, we discuss the role of
causation in explanation, and posit and bound a proposed target of inference
called the probability of explanation (PE). In \S \ref{sec:estimation}, we
discuss how bounds may be efficiently estimated from observed data. Finally, in
\S \ref{sec:conclusion}, we conclude the thesis proposal.

\end{document}
