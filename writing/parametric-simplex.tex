\documentclass[main.tex]{subfiles}

\begin{document}

\section{Symbolic Solutions to Parameterized Linear Programs}
\label{sec:parametric-simplex}

In \S \ref{sec:measurement-error}, we showed that a number of problems
involving measurement error can be framed as linear programs. Progress has also
been made recently on characterizing causal queries that can be so expressed
\cite{sachs2020linear}. In most cases, the linear programs are most naturally
expressed in the dual form described in Equation (\ref{eq:dual-lp}), as the
distributional constraints discussed are equality constraints, and the
parameters of probability distributions that serve as variables in the programs,
that is $\phi$ of the preceding section, are definitionally non-negative.

It is clear that the $c$ vector in such linear programs will depend on the
observed data distribution, as the joint distribution under consideration is
constrainted to marginalize to the observed data distribution. If the $b$ vector
does not refer to the observed data distribution, the linear program can be put
into its primal form, in which case the primal polytope is defined without
reference to the observed data distribution, and can be subjected to a standard
vertex-enumeration algorithm. Because the optimimum is known to
occur at a vertex, the solution to the problem will be the maximum value the
objective takes at any vertex, yielding the solution as a function of the
observed data distribution. This was the approach proposed in
\cite{balke1997thesis}, and used to obtain the original IV bounds.

However, if the $b$ vector also contains reference to the observed data
distribution, this solution is no longer available, as the primal polytope
itself becomes a function of the observed data distribution. This situation
arises if, for example, we are interested in the factual distribution of $X$ in
the instrumental variable scenario with measurement error described above,
rather than in the counterfactual distribution $X(a)$. This is simply because
the factual distribution $P(X)$ is an average of $P(X(a))$ and $P(X(\bar a))$,
weighted by $P(A)$, which is part of the observed data distribution. The same
issue arises in more complex scenarios as well.

Obtaining solutions to these linear programs in terms of the observed data
distribution is important for three reasons. First, it is evident from the
adoption of the IV bounds that they are used in settings and by practitioners
who are not equipped to set up and run linear programs. We believe in general
that symbolic solutions will see broader adoption than numeric procedures.
Second, symbolic solutions allow theorists to gain insights into the structure
of the underlying problem. Some of the methods developed in \S
\ref{sec:causal-bounds}, for example, were inspired by careful study of the
symbolic IV bounds obtained through the vertex enumeration approach. Finally,
the observed data distribution, used in the expression of the linear program, is
known only up to statistical uncertainty. It is not well understood how such
uncertainty can be propogated through a numeric optimization procedure. By
contrast, estimation of explicit functionals of the observed data distribution
is well understood.

The standard simplex algorithm has an intuitive conceptual explanation.
Beginning at one vertex of the primal polytope, at each iteration it moves to an
adjacent vertex at which the objective is greater, until no such adjacent vertex
exists. When there are multiple candidate vertices to visit at some iteration,
the algorithm chooses one by a user-specified rule. The algorithm
is initialized at a vertex of the polytope, represented as a matrix often
referred to as a \textit{tableau}. Moves between adjacent vertices are
represented by a matrix operation referred to as a \textit{pivot}, in which
variables are selected to enter and leave the basis of the tableau. The pair of
entering and leaving variables can be interpretted as specifying which adjacent
vertex is to be visited at the next iteration of the algorithm.

To address problems in which both $b$ and $c$ are parameterized by the observed
data distribution, we propose an extension to the simplex algorithm called the
parametric simplex algorithm, which can be used to provide solutions to
parameterized linear programs. We distinguish between the \textit{variables} of
the linear program, which in our setting correspond to the unobserved
distribution $\phi$ we are optimizing over, and its \textit{parameters}, which
in our setting correspond to the observed data distribution $P({\bf V})$. A
solution to a parameterized linear program should provide the optimum in terms
of the parameter values.

The simplex algorithm has only one decision point in each iteration, which is
the point at which it selects which vertex to visit next. In our setting, this
decision may depend on the parameters of the observed data distribution. Our
goal is simply to keep track of which parameter values correspond to which
decisions. We do so by constructing a tree, where each node in the tree is
associated with (i) a subspace of possible parameter values and (ii) the tableau
constructed by pivoting the entering and leaving variables corresponding to that
subspace in its parent's tableau. The path to each leaf node represents the
trajectory that the simplex algorithm would take for a fully numeric linear
program when the parameters of the program take values in the subspace
associated with the node.

\begin{algorithm}[h!]
	\caption{Parametric Simplex}
  \label{alg:parametric-simplex}
  \include{parametric-simplex-algorithm}
\end{algorithm}

We include pseudo-code for the parametric simplex in Algorithm
\ref{alg:parametric-simplex}. In this presentation, $\mathcal F$ represents the
subspace of possible parameter values. In our setting, recall that the
parameters of the linear program are the parameters of the observed data
distribution, and so are non-negative and must sum to one. In other settings,
there may be domain reasons to restrict the possible parameters.

We suppose we have access to $entering$ and $leaving$ functions, that take as
input a tableau and a setting of parameter values, and returns entering and
leaving variables respectively. In practice, the sets constructed in lines $8$
and $10$ can be expressed as a system of inequalities involving $\phi$. If an
entering variable cannot be identified, the present tableau must represent an
optimum. If an entering variable can be identified, but a leaving variable
cannot, the objective is unbounded. For all other regions of the space, both an
entering and a leaving variable can be identified.

Once the algorithm is complete, its output may be used in two ways. First, its
leaf nodes may be examined to provide a symbmolic solution to an optimization
problem in terms of the parameters of the linear program. In our case, this
corresponds to presenting an optimum in terms of the observed data distribution.
Second, the output tree may be queried to find numeric solutions to the
parameterized linear program for certain parameter values. In this case, the
tree can be treated as a decision tree. Beginning at the root node, we identify
the child node corresponding to the parameter subspace in which the specified
parameter values are located, and repeat until we get to a leaf node. We believe
that this second use will be helpful in situations where a large linear program
with few parameters is run repeatedly for different parameter values.

We also hypothesize that even when the vertex enumeration approach is available,
there are some cases in which the parametric simplex algorithm will be faster in
practice. The reason for this is that the vertex enumeration approach must
enumerate all vertices, including those that the simplex algorithm would not
visit for any setting of the parameters. These savings must be weighted against
the fact that the parametric simplex algorithm may visit the same vertex at
multiple points in the tree. We plan to evaluate these relative merits
empirically through computational experiment.

\subsubsection*{Status}

This works is being prepared for submission. During the course of this
work we also considered parametric forms of optimization procedures for
non-linear constraints, which arise in more complex causal models, but did not
obtain any useful results. We may return to this question at a later time.

\end{document}
