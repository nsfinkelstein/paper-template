\documentclass[main.tex]{subfiles}

\begin{document}

\section{Causal Reasoning for Generating Explanations}
\label{sec:explanation}

The literature around causal inference has long claimed that it can answer
questions about \textit{why} events transpired. The actual machinery of causal
inference, though, is largely focused on questions about how outcomes will be
affected by different interventions. This is perhaps because absolute answers to
such questions are unobtainable, and even probabilistic answers to such
questions are rarely point-identified without strong assumptions. We propose
methods for bounding such quantities, using ideas similar to those explored
above.

Our goal is to develop a system that can automatically generate the ``best
possible'' explanation for a recorded event given observed data, where the users
of the system can express preferences for different features of the explanation
by setting system parameters. We are particularly interested in explaining
unexpected patient outcomes in a hospital setting using EMR data. We hope
such explanations may help clinicians develop a deeper understanding of why such
outcomes came about.

A key part of generating explanations that will be of use to clinicians will
be developing an understanding of what kinds of explanations clinicians find
most useful. It is well known that eliciting such information is not as
straightfoward as asking the parties involved for their preferences
\cite{huldtgren2010elicitation}. For that reason, we hope to find volunteers to
answer surverys in which they are presented with unexpected phenomena, and asked
to reflect on provided explanations, or to describe the kinds of explanations
they would accept.

In addition, we draw on the philosophical literature on explanation to consruct
a explanatory target of inference. A popular theory of causal
explanation states that an outcome $Y = y$ is explained by a treatment $A = a$
if $Y = y$ would not have occurred, had $A = a$ not occurred
\cite{woodward2003making}. There are, of course, many ways for $A = a$ to not
occur. Proponents of ``contrastive explanation'' \cite{lipton1990contrastive}
posit that all explanations either explicitly or implicitly have an alternative
treatment value in mind, i.e. $A = a'$, with $a \neq a'$. Many theories of
explanation assume deterministic systems \cite{salmon1998causality} or systems in
which the structural equations, or a distribution over sets of structural
equations, are known \cite{halper2016actual}, which we consider to be of limited
use in real-world data.

A simple probabilistic extension of the causal, contrastive view of explanation
acknowledges that in stochastic systems it is impossible to know what would
have happened had treatment been changed, but that it may be possible to
identify or bound the \textit{probability} that the outcome would have changed.
In the simplest case of two binary variables, this approach suggests the
following target as the probability that $A = a$ \textit{explains} $Y = y$:
%
\begin{align}
  P(Y(\bar a) = \bar y \mid Y = y, A = a).
\end{align}
%
\noindent This target has been called the Probability of Necessity (PN), and
bounded using a linear programming procedure \cite{tian1999causation}. This
probability is in essence a cross-world probability; it asks what proportion of
those who in fact experience $Y = y, A = a$ would have experienced $Y(\bar a) =
\bar y$. The difficulty is that by construction, we cannot see what any
individual who experiences $Y = y, A = a$ would have experienced under
intervention $A = \bar a$.

In our setting, the treatment can take many possible values. In addition, we
are likely to have observed many additional variables. For convenience, we
denote all remaining observed variables $\bf C$, and propose the following
extension of the PN target, which we call the Probability of Explanation (PE):

\begin{align}
  \max_{a' \neq a} P(Y(a') \neq y \mid Y = y, A = a, {\bf C = c}),
\end{align}

\noindent where $y, a$ and $\bf c$ are the observed values of $Y, A$ and $\bf C$
respectively. This quantity can be interpreted as an answer to the question:
among the population with all the same properties as the individual in question,
what portion would not have experienced $Y = y$ had we intervened to set $A =
a'$, rather than letting $A$ take its natural value of $a$?

We now develop a bound on the PE in the case where treatment is conditionally
ignorable, i.e. $A \perp Y(a) \mid {\bf C}$. This approach recovers the linear
programming bounds on the PN without relying on the linearity of the target, but
can be applied to more complex scenarios. First, we rewrite the maximand of the
PE to isolate the cross-world component.

\begin{align}
  P(Y(a') \neq y \mid Y = y, A = a, {\bf C = c})
  &= \frac{P(Y(a') \neq y, Y = y, A = a, {\bf C = c})}{P(Y = y, A = a, {\bf C = c})}\\
  &= \frac{P(Y(a') \neq y, Y(a) = y, A = a, {\bf C = c})}{P(Y = y, A = a, {\bf C = c})}\\
  &= \frac{P(Y(a') \neq y, Y(a) = y \mid {\bf C = c})P({\bf C = c}, A = a)}{P(Y = y, A = a, {\bf C = c})}.
\end{align}

The penultimate step is by causal consistency, and the final step is due to
conditional ignorability. This formulation makes clear that the only component
of this target that is not identified is $P(Y(a') \neq y, Y(a) = y \mid {\bf C =
  c})$. Because $Y(a)$ and $Y(a')$ are arbitarily confounded, and there are no
other variables to the left of the conditioning bar, the sharp bounds on this
density are simply the Fr\'echet bounds, which yield

\begin{align}
  \label{eq:bounds}
  P(Y(a') \neq y, Y(a) = y \mid {\bf C = c}) &\ge \max \{0, P(Y(a') \neq y \mid {\bf C = c}) + P(Y(a) = y \mid {\bf C = c}) - 1 \}\\
  \nonumber
  P(Y(a') \neq y, Y(a) = y \mid {\bf C = c}) &\le \min \{P(Y(a') \neq y \mid {\bf C = c}), P(Y(a) = y \mid {\bf C = c})\}.
\end{align*}

Inserting these bounds appropriately into the maximand along with some simple
algebraic manipulation, and taking advantage again of conditional ignorability, yields

\begin{align}
  \label{eq:bounds}
  P(Y(a') \neq y \mid Y = y, a = a, {\bf C = c}) &\ge \max \{0, 1 - \frac{P(Y = y \mid A = a', {\bf C = c})}{P(Y = y \mid A = a,{\bf C = c})}\}\\
  \nonumber
  P(Y(a') \neq y \mid Y = y, a = a, {\bf C = c}) &\le \min \{1, \frac{1 - P(Y = y \mid A = a', {\bf C = c})}{P(Y = y \mid A = a, {\bf C = c})}\},
\end{align*}

\noindent recovering sharp bounds in the linear case. Similar bounds can be
constructed in the absence of conditional ignorability. In this case, we rewrite
the maximand in the PE as

\begin{align}
  P(Y(a') \neq y \mid Y = y, A = a, {\bf C = c})
  = \frac{P(Y(a') \neq y, Y = y, A = a \mid {\bf C = c})P({\bf C = c})}{P(Y = y, A = a, {\bf C = c})}.
\end{align}

The Fr\'echet bounds applied to $P(Y(a') \neq y, Y = y, A = a \mid {\bf C = c})$
yield

\begin{align}
  \label{eq:pe-nonignorable}
  P(Y(a') \neq y, Y(a) = y \mid {\bf C = c}) &\ge \max \{0, P(Y(a') \neq y \mid {\bf C = c}) + P(Y = y, A = a \mid {\bf C = c}) - 1 \}\\
  \nonumber
  P(Y(a') \neq y, Y(a) = y \mid {\bf C = c}) &\le \min \{P(Y(a') \neq y \mid {\bf C = c}), P(Y = y, A = a \mid {\bf C = c})\}.
\end{align*}

In this case, $P(Y(a') \neq y \mid {\bf C = c})$ is not identified, but it may
be substantively bounded. For example, in the IV scenario, the potential outcome
$Y(a)$ is marginally independent of the instrument $Z$, so that the standard
bounds IV bounds for $P(Y(a') \neq y)$ can be substituted in to Equation
(\ref{eq:pe-nonignorable}). Propogating these bounds through the Fr\'echet
bounds into bounds on the maximand of the PE is straightforward.

\subsubsection*{Accomodating user preferences about explanations}

In seeking an explanation for an event $Y = y$, we do not know ahead of time
which treatment will yield the largest PE (or the largest bound on the PE).
We will therefore need to calculate this bound for any of a number of
treatments. The user may have preferences about the kinds of
treatments that can be considered to be explanatory, and whether sets of
treatments may be jointly considered. The user may also have preferences about
how to trade-off between the number of treatments considered and the PE. For
example, they may decide that an explanation based on a large number of
treatments may be ``worse'' than an explanation based on a small number,
even if the former has a slightly higher PE.

Similarly, the user may have preferences about which kind of contrastive
explanations are considered. As noted above, the value $a'$ that maximizes the
maximand in the PE plays a role in the contrastive explanatory interpretation of
the PE. If the probability that $A$ takes this value is very small, the
explanation may be seen to fail. For example, suppose we want to explain why a
car crashed on a highway. The PE may reflect that the car would not have crashed
had it been driving at 15 miles per hour. However, it is highly unusual for a
car to be driving at 15 miles per hour, conditional on being on a highway in the
absence of traffic. Users of the system may want to discount this kind of
explanatory contrast as compared to contrasts with more probable treatment
settings.

We are aware that interactions with potential users of the system may well lead
to the recognition that there are other important dimensions along which users
would like to express their preferences about the kinds of explanations they
receive from the system. It may emerge that the target we identified above is
not well suited to delivering useful information to clinicans. We follow the
widely held philosophical perspective that explanation is fundamentally a
counteractual task; if the PE is not the appropriate target, we will
endeavor to articulate -- and bound -- a more fitting counterfactual parameter.

\subsubsection*{Status}

The work in this section is currently under deveopment. We have had very
preliminary conversations with clinicians on this topic.

\end{document}
