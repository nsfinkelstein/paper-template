\documentclass[main.tex]{subfiles}

\begin{document}

\section{Partial Identification for Missing Not at Random Data}
\label{sec:missingness-bounds}

\begin{figure}[h!]
  \centering
    \begin{tikzpicture}[>=stealth, node distance=1.75cm]
      \tikzstyle{vertex} = [
      draw, very thick, circle, minimum size=7.5mm, inner sep=0pt
      ]
      \tikzstyle{edge} = [
      ->, blue, very thick
      ]

      \node[vertex] (x11) {$X^*_1$};
      \node[vertex] (x12) [right of=a] {$X^*_2$};
      \node[vertex] (r1) [below of=x11] {$R_1$};
      \node[vertex] (r2) [below of=x12] {$R_2$};
      \node[vertex] (x1) [below of=r1] {$X_1$};
      \node[vertex] (x2) [below of=r2] {$X_2$};

      \draw[edge] (x11) to (r2);
      \draw[edge] (x12) to (r1);
      \draw[edge] (x11) to (x12);
      \draw[edge] (r1) to (r2);
      \draw[edge, gray] (r1) to (x1);
      \draw[edge, gray] (r2) to (x2);
      \draw[edge, gray] (x11) [bend right=40] to (x1);
      \draw[edge, gray] (x12) [bend left=40] to (x2);
       
  \end{tikzpicture}
    \caption{
      Simple missingness model in which $P(X^*_1, X^*_2)$ is not identified.
    }
    \label{fig:missing-data}
\end{figure}

In this section, we explore missingness models that can be represented as
graphical models. In this paradigm, we have a set of true variables of
interest, ${\bf X^*} \equiv X^*_1, \dots, X^*_N$. However, we do not observe
these variables directly, and instead of observe variables ${\bf X} \equiv X_1,
\dots, X_N$, where $X_i = X^*_i$ if the observation is not missing, and $X_i =
?$ otherwise. Each missing variable has a missingness indicator $R_i$ that
denotes whether that variable is observed or missing, such that $X_i$ can be
expressed as
%
\begin{align}
  X_i = \begin{cases}
    X_i^* & \text{if~} R_i = 1 \\
    ? & \text{if~} R_i = 0.
  \end{cases} 
\end{align}
%
We are often interested in the distribution $P({\bf X^*})$. We can reason about
the distribution of $P(X^*_i)$ causally by reasoning about the hypothetical
world in which we intervene to set the missingness indicator $R_i = 1$. In such
a hypothetical world, $X^*_i = X_i(R_i = 1)$, and because $X_i$ is observed, the
distribution $P(X^*_i)$ is known. It is important to note that missingness
indicators can be causes of other missingness indicators, such that the
potential outcome $X_i(R_j = 1)$ for $i \neq j$ is not necessary the same as
$X_i$ in the factual world.

Graphically, these models can be represented as in Figure \ref{fig:missing-data}
\cite{mohan2013missing}. The determination of $X_i$ by $X^*_i$ and $R_i$ is
represented by the grey edges in the graph. In causal inference without missing
values, if a treatment $A$ satisfies certain conditions, the distribution over
all other  variables $\bf V$ in the hypothetical world in which $A$ is set
exogenously to $a$ is given by
%
\begin{align}
  \label{eq:fixing}
  q_{A = a}({\bf V = v}) = \frac{P({\bf V = v}, A = a)}{P(A = a \mid {{\bf V}_{pa_{\mathcal G}(A)} = {\bf v}_{pa_{\mathcal G}(A)}})}.
\end{align}
%
The same reasoning applies for intervention on missingness indicators $R_i$.
However, difficulty arises if a parent of $R_i$ is some variable $X_j^*$. In
such cases, the denomenator of Equation (\ref{eq:fixing}), $P(R_i \mid
pa_{\mathcal G}(R_i))$, can only be evaluated where $X_j^*$ is observed, i.e. in
events with $R_j = 1$. In past work this has been termed ``inducing selection
bias on $R_j$'' \cite{rozi2019mid}. As a result of this selection bias, events
in the outcome space in the hypothetical world under intervention on $R_i$ in
which $R_j$ have unknown probability. This lack of identification blocks exact
identification of distributions in hypothetical worlds under further
intervention, i.e. in which additional indicators are set to $1$.

In this work, we propose to study how much we can learn about the probability
distribution in hypothetical worlds where calculating the probability
distribution in those worlds induces selection bias. In addition, we will study
how any such information can be propogated through to inform our understanding
of distirbutions on further interventions on missingness indicators. Below we
provide a simple analysis of how this might work.

\subsubsection*{Partial identification of distributions under selection bias}

\begin{table}
  \centering
    \begin{tabu}{| c | c | c | c | c | c |[2pt] c|} \hline
      $X^*_2$ & $X^*_2$ & $R_1$ & $R_2$ & $X_1$ & $X_2$ & $P$ \\\tabucline[2pt]{-}
      1 & 1  & 1 & 1 & 1 & 1 & $\phi_0 = P(X_1 = 1, X_2 = 1)$ \\ \hline
      1 & 1  & 1 & 0 & 1 & ? & $\phi_1  \in \left[ 0, P(X_1 = 1, X_2 = ?)\right]$ \\ \hline
      1 & 1  & 0 & 1 & ? & 1 & $\phi_2  \in \left[ 0, P(X_1 = ?, X_2 = 1)\right]$ \\ \hline
      1 & 1  & 0 & 0 & ? & ? & $\phi_3 \in \left[ 0, P(X_1 = ?, X_2 = ?) \right] $ \\ \hline
      1 & 0  & 1 & 1 & 1 & 0 & $\phi_4 = P(X_1 = 1, X_0 = 0)$ \\ \hline
      1 & 0  & 1 & 0 & 1 & ? & $\phi_{5} \in \left[0, P(X_1 = 1, X_2 = ?)\right$ \\ \hline
      1 & 0  & 0 & 1 & ? & 0 & $\phi_{6} \in \left[0, P(X_1 = ?, X_2 = 0)\right$ \\ \hline               
      1 & 0  & 0 & 0 & ? & ? & $\phi_{7} \in \left[ 0, P(X_1 = ?, X_2 = ?) \right] $ \\ \hline
      0 & 1  & 1 & 1 & 0 & 1 & $\phi_8 = P(X_1 = 0, X_0 = 1)$ \\ \hline
      0 & 1  & 1 & 0 & 0 & ? & $\phi_{9} \in \left[0, P(X_1 = 0, X_2 = ?)\right$ \\ \hline
      0 & 1  & 0 & 1 & ? & 1 & $\phi_{10} \in \left[0, P(X_1 = ?, X_2 = 1)\right$ \\ \hline
      0 & 1  & 0 & 0 & ? & ? & $\phi_{11} \in \left[ 0, P(X_1 = ?, X_2 = ?) \right] $ \\ \hline
      0 & 0  & 1 & 1 & 0 & 0 & $\phi_{12} = P(X_1 = 0, X_2 = 0)$ \\ \hline
      0 & 0  & 1 & 0 & 0 & ? & $\phi_{13} \in \left[0, P(X_1 = 0, X_2 = ?)\right$ \\ \hline
      0 & 0  & 0 & 1 & ? & 0 & $\phi_{14} \in \left[0, P(X_1 = ?, X_2 = 0)\right$ \\ \hline
      0 & 0  & 0 & 0 & ? & ? & $\phi_{15} \in \left[ 0, P(X_1 = ?, X_2 = ?) \right] $ \\ \hline
    \end{tabu}
    \caption{
      Event Space and Probability Distribution for a two variable missing data
      model.
    }
    \label{tab:distribution}
\end{table}

Consider Table \ref{tab:distribution}, which lists the events in the otucome
space of the factual world corresponding to Figure \ref{fig:missing-data}. The
parameters of the distribution in this world are denoted by $\phi$. Because some
of the events in the factual world look the same -- e.g. all four events in
which $R_1 = R_2 = 0$ have the observed variables $X_1 = X_2 = ?$ -- the
parameters $\phi$ are not exactly identified for some events. However even when
they are not identified, they can be bounded. For example, no event in which
$R_1 = R_2 = 0$ can have probability greater than $P(X_1 = ?, X_2 = ?)$, because
$R_1 = R_2 = 0 \implies X_1 = X_2 = ?$. These bounds are reflected in the table.
In addition to these bounds, certain linear constraints on $\phi$ obtain. For
example, the sum of $\phi$ parameters corresponding to events in which $R_1 =
R_2 = 0$ must equal the observed probability that $P(X_1 = ?, X_2 = ?)$.

\begin{table}[h!]
  \centering
    \begin{tabu}{| c | c | c | c | c | c |[2pt] c|} \hline
      $X^*_2$ & $X^*_2$ & $R_1$ & $R_2$ & $X_1$ & $X_2$ & $P$ \\\tabucline[2pt]{-}
      1 & 1  & 1 & 1 & 1 & 1 & $\alpha_0 = \frac{P(X_1 = 1, X_2 = 1)}{P(X_2 \neq ? \mid X_1 = 1)}$ \\ \hline
      1 & 1  & 0 & 1 & ? & 1 & $\alpha_1  \in \left[ 0, \frac{P(X_1 = ?, X_2 = 1)}{P(X_2 \neq ? \mid X_1 = ?, X^*_1 = 1)}\right]$ \\ \hline
      1 & 0  & 1 & 1 & 1 & 0 & $\alpha_2 = \frac{P(X_1 = 1, X_2 = 0)}{P(X_2 \neq ? \mid X_1 = 1)}$ \\ \hline
      1 & 0  & 0 & 1 & ? & 0 & $\alpha_{3} \in \left[0, \frac{P(X_1 = ?, X_2 = 0)}{P(X_2 \neq ? \mid X_1 = ?, X_1^* = 1)}\right]$ \\ \hline               
      0 & 1  & 1 & 1 & 0 & 1 & $\alpha_4 = \frac{P(X_1 = 0, X_2 = 1)}{P(X_2 \neq ? \mid X_1 = 0)}$ \\ \hline
      0 & 1  & 0 & 1 & ? & 1 & $\alpha_{5} \in \left[0, \frac{P(X_1 = ?, X_2 = 1)}{P(X_2 \neq ? \mid X_1 = ?, X_1^* = 0)}\right]$ \\ \hline
      0 & 0  & 1 & 1 & 0 & 0 & $\alpha_{6} = \frac{P(X_1 = 0, X_2 = 0)}{P(X_2 \neq ? \mid X_1 = 0)}$ \\ \hline
      0 & 0  & 0 & 1 & ? & 0 & $\alpha_{7} \in \left[0, \frac{P(X_1 = ?, X_2 = 0)}{P(X_2 \neq ? \mid X_1 = ?, X_1^* = 0)}\right]$ \\ \hline
    \end{tabu}
    \caption{
      Event Space and Probability Distribution for after intervening to set $R_2 = 2$
      model.
    }
    \label{tab:intervention}
\end{table}

Suppose now we are interested in the distribution after intervention $R_2 = 1$.
We calculate the probabilities of each event in the outcome space of this world 
according to Equation (\ref{eq:fixing}), and the results are displayed in Table
\ref{tab:intervention}. The parameters of the distribution in this world are
denoted by $\alpha$. Events with $R_1 = 1$ have exactly identified densities.
Events with $R_1 = 0$ cannot have exactly identified densities, because the
corresponding events in the factual world did not have identified densities.
However, the situation is even more dire now because the denominator for their
upper bounds is $P(X_2 \neq ? \mid X = ?, X^*_1 = x_1)$, which conditions on the
unobserved value of $X^*_1$. This value can be bounded in terms of $P(X_1^* =
0)$ by the Fr\'echet bounds, which in turn can be bounded by using a consistency
argument described below.

In addition, in this case by the nature of the operation described in Equation
(\ref{eq:fixing}), there exists a marginalization relationship between $\alpha$
and $\phi$. For example, $\alpha_1 = \phi_2 + \phi_3$, as intervening on $R_2$
will not change the factual outcomes of $R_1, X_1^*$ or $X_2^*$. This
relationship, along with the constraints on $\phi$ implied by the observed data
distribution mentioned above constrain the unidentified $\alpha$ parameters in
terms of the observed data distribution as well.

Note that the 4 unidentified $\alpha$ parameters represent only two degrees of
freedom, as two degrees are consumed by the observed data distribution
constraints and the independence constraint. This suggets that setting two of
these parameters is sufficient to fully identify the distribution, and therefore
the target $P(X^*_1, X^*_2)$. These can be used as sensitivity parameters and
varied within a plausible range.

We plan to explore the constraints we can place on distributions like the one
displayed in Table \ref{tab:intervention} under different models in detail, and
to study how these constraints can be propogated through to provide information
about distributions under further interventions. We are also interested in how
we might perform sensitivity analyses in such settings, where the analysis would
be with respect to the sensitivity of inference to the degree of difference
between observed and unobserved values.

\subsubsection*{Consistency bounds in missing data}

Suppose we are able to obtain the distribution under intervention on a subset of
missingness indicators $\bf \tilde R \subseteq R$. Even if we have induced
selection bias, as described above, the events in the outcome space
in which $\bf R = 1$ will be exactly identified. In this case, a simple
lower bound for $P({\bf X^* = x})$ is $P({\bf X(\tilde R = 1) = x})$. This bound
is a result of the fact that $\bf X(\rilde R = 1) \implies X^* = x$ through
consistency and, in this case, the deterministic relationship between $X_i^*$,
$R_i$ and $X_i$.

To illustrate the possible utility of this simple fact, imagine a two-variable
scenario in which it is possible to intervene on $R_1$, but not on $R_2$.
Suppose further that there is a high level of missingness in both variables,
such that the consistency bounds applied in the factual world are very
uninformative. After intervention on $R_1$, $X_1$ becomes fully observed. If in
addition $R_1$ is a parent of $R_2$, it is possible that setting $R_1 = 1$ will
reduce missingness in $X_2$. In such cases, the consistency bounds applied after
intervention on $R_1$ may be quite informative.

Finally, we discuss how cross-world reasoning can be used in this scenario to
obtain better bounds, much as it was used in the IV scenario described above.
Suppose that it is possible to identify distributions in hypothetical worlds
under intervention on $\bf R_1$ and $\bf R_2$, which are distinct (though not
necessarily disjoint) sets of indicators. According to the reasoning above, each
of these distributions may be used to find a bound on the target distribution
$\bf X^*$. However in some cases, it may be possible to obtain a tighter bound
by considering both worlds at once.

Any valid lower bound on $P({\bf X(R_1 = 1) = x \lor X(R_2 = 1) = x})$ would,
again by consistency, be a valid lower bound on $P({\bf X^* = x})$. A lower
bound on this cross-world probability can be obtained through a variant of the
Fr\'echet bounds. We note, though, that the analog of this approach would
not yield sharp bounds in the IV scenario.

We therefore also consider an alternative approach to using information from
both worlds, which is similar to the approach that did yield sharp bounds
in the IV scenario. Under this approach, we select a value of $\bf X = x'$, with
$\bf x \neq x'$, and bound $P({\bf X(R_1 = 1) = x}) + P({\bf X(R_1 = 1) = x',
  X(R_2 = 1) = x})$. The first term is identified by assumption. The second term
represents the portion of the population who experience $\bf X(R_2 = 1) = x$
under $\bf R_2 = 1$, but $\bf X(R_1 = 1) = x'$ under $\bf R_1 = 1$, and can
again be bounded by the Fr\'echet inequalities. Note that $x'$ may include $?$
values.

\subsubsection*{Status}

This work is very preliminary. We aim to discover which of the ideas
explored above will yield informative bounds, and under which conditions. In
addition, we believe there may be other techniques available along similar lines
to obtain bounds in missing data scenarios. We hope to combine these techniques
into an algorithm that can return bounds on the target distribution in arbitrary
causal graphs with missing data.

\end{document}
