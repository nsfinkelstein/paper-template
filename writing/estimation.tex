\documentclass[main.tex]{subfiles}

\begin{document}

\section{Efficient Estimation of Partial Identification Regions}
\label{sec:estimation}

Up to this point, we have discussed the parameters of the observed data
distribution $P({\bf V})$ as though they were known to us exactly.
Unfortunately, this distribution is known only up to statistical uncertainty,
and functionals of interest must be estimated from data. A large literature on
semiparametric and nonparametric statistical estimation advocates for the use of
influence functions, which often lead to estimators with desirable properties
such as fast rates of convergence \cite{scharfstein1999double}. Influence
functions are closely related to ``pathwise derivatives'' of an estimand
\cite{newey1993pathwise}, which quantify the change in the estimand as the
empirical distribution moves in the direction of the true distribution
\cite{fisher2020influence}. Many bounds on causal parameters are best expressed
as maxima or minima over other functionals of the observed data law, and as such
are non-differentiable. In this section, we explore an approach to the
estimation of such bounds that allows for the use of influence functions by
relaxing the bounds.

Formally, suppose $\{\phi_i(P)\}_i$ is a set of pathwise differentiable
functionals of the observed data distribution $P$, and that $P(Y(a) = y) \ge \max
\{\phi_i(P)\}_i$. This implies that $P(Y(a) = y)$ is greater than any weighted
average over $\{\phi_i(P)\}$. In other words, for any set of weights
$\{\alpha_i\}_i$ such that $\alpha_i \ge 0$ for all $i$, and $\sum_i \alpha_i =
1$,
% 
\begin{align*}
  P(Y(a) = y) \ge \max \{\phi_i(P)\}_i \implies P(Y(a) = y) \ge \sum_i \alpha_i \phi_i(P).
\end{align*}
% 
Although $\max \{\phi_i(P)\}_i$ is not pathwise differentiable, $\sum_i \alpha_i
\phi_i(P)$ is. This suggests that for any choice of $\alpha_i$ it is possible
to find an efficient influence function that will yield valid bounds on
$P(Y(a) = y)$, leaving the question of how $\alpha_i$ should be selected.

The $\max$ operation can be thought of as a weighted average in which all weight
is placed on the largest element of the set, and no weight is placed elsewhere.
We would like to select settings of the weights that come close to this ideal,
without introducing discontinuities into the pathwise derivative of the
functional. To that end, we suggest the following weights
\begin{align}
  \label{eq:weights}
  \alpha_i(\alpha_0) =
  \frac{\exp(\alpha_0 \phi_i(P))}{\sum_j \exp(\alpha_0 \phi_j(P))},
\end{align}
where $\alpha_0$ is a parameter that allows the analyst to trade-off between the
sharpness of the bounds and the stability of the estimator. This leads to
the following target functional, representing a valid bound:
\begin{align}
  \label{eq:relaxed-if}
  \psi(\alpha_0) = \sum_i
  \frac{\exp(\alpha_0 \phi_i(P))}{\sum_j \exp(\alpha_0 \phi_j(P))}
  \phi_i(P_0).
\end{align}
\noindent This formulation is closely related to the softmax function
\cite{gibbs1902softmax}, which is widely used in other statistical applications where
differentiable alternatives to the $\max$ function are important
\cite{goodfellow2017deep}.

To understand the tradeoff between stability and sharpness enabled by
$\alpha_0$, we first note that as $\alpha_0$ increases, $\psi(\alpha_0)$
approaches the $\max$ function, i.e.
\begin{align}
  \lim_{\alpha_0 \rightarrow \infty} \psi(\alpha_0) =
  \max \{\phi_i(P)\}_i.
\end{align}
When $\alpha_0$ is very large, the bounds are nearly as sharp as the original
functional $\max \{\phi_i(P)\}_i$, as nearly all the weight is placed on the
largest functional. However, this will yield near-infinte gradients at points
where the gradient of the $\max$ function are undefined, which in turn will
yield unstable one-step estimators. By contrast, when $\alpha_0$ is small the
bounds are less sharp, as more weight is placed on smaller values. However
gradients are not as explosive at certain distributions, yielding a more stable
estimator. We plan to explore this theoretical tradeoff between estimator
properties in simulation studies.

In deriving an influence function for the relaxed bounds of Equation
(\ref{eq:relaxed-if}), we make use of the fact that
%
\begin{align}
    \label{eq:if-id}
\frac{\partial}{\partial \epsilon}
\psi(\alpha_0) = \mathbb{E}\left[ U(\psi(\alpha_0)) \times S({\bf V}) \right],
\end{align}
%
where $U(\psi(\alpha_0))$ is the influence function for the target functional,
and $S({\bf V})$ is the score function for all variables in the system $\bf V$
\cite{tsiatis2006semiparam}. The pathwise derivative of $\psi(\alpha_0)$, the functional
representing the relaxed bounds, can now be expressed in terms of the pathwise
derivatives of each functional in the set $\{\phi_i\}_i$ as follows:
% 
\begin{align}
  \label{eq:bounds-pathwise-derivative}
  \frac{\partial}{\partial \epsilon}
  \psi(\alpha_0)
   =
  \sum_i \bigg(
  &\frac{
    \exp(\alpha_0 \phi_i(P))
    \frac{\partial \phi_j(P_{\epsilon})}{\partial \epsilon}
  }{
  \sum_j \exp(\alpha_0 \phi_j(P))
  }
  \\\nonumber
  &+
  \frac{
     \frac{\partial \phi_i(P_{\epsilon})}{\partial \epsilon}
     \alpha_0 \exp(\alpha_0 \phi_i(P))\phi_i(P)
   }{
     \sum_j \exp(\alpha_0 \phi_j(P))
   }
  \\\nonumber
  &+
  \frac{
    \exp(\alpha_0 \phi_i(P))\phi_i(P) \sum_j
    \frac{\partial \phi_j(P_{\epsilon})}{\partial \epsilon}
    \alpha_0 \exp(\alpha_0 \phi_j(P))
  }
  {
    \big(\sum_j \exp(\alpha_0 \phi_j(P))\big)^2
  }\bigg).
\end{align}

This pathwise derivative can be used to obtain the influence function for the
$\max$ over a set of functionals, though the derivation will in general depend
on the form of the functionals. In the remainder of this section, we derive the
influence function for a simple but important class of functionals.

\subsubsection*{Influence Function for Simple Bounds}

Assume that all functionals $\phi_i(P)$ are simple sums of marginal or
conditional probabilities, as in the mediator bounds of Equation
(\ref{eq:mediator-bounds}) and the IV bounds of Equation (\ref{eq:iv-bounds}}).
Then we can express each functional in the maximand set as
% 
\begin{align}
  \phi_i(P) = \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}),
\end{align}
%
\noindent where ${\bf A}_{ik} \cap {\bf B}_{ik} = \emptyset$ and ${\bf B}_{ik}$
can be the empty set. Let $Z$ represent the normalizer $\sum_{i} \exp(\alpha_0
\sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}))$. Then for the $i^{th}$ functional,
we consider each term in the sum in the pathwise derivative
(\ref{eq:bounds-pathwise-derivative}) in turn. We begin with the first term,
which can now be written
%
\begin{align*}
  \frac{
    \exp(\alpha_0 \phi_i(P))
    \frac{\partial \phi_j(P_{\epsilon})}{\partial \epsilon}
  }{
  \sum_j \exp(\alpha_0 \phi_j(P))
  }
  =
  \frac{\exp(\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}))}{Z}
  \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik})S({\bf a}_{ik} \mid {\bf b}_{ik}).
\end{align*}
%
Observe that the $k^{th}$ term in the sum above, $P({\bf a}_{ik} \mid {\bf b}_{ik})S({\bf a}_{ik} \mid {\bf b}_{ik})$, can be expressed
%
\begin{align*}
    \mathbb{E}\bigg[
      (\frac{\mathbb{I}[{\bf b}_{ik}' = {\bf b}_{ik}, {\bf a}_{ik}' = {\bf a}_{ik}]}{P({\bf b}_{ij})} -
      P({\bf a}_{ik} \mid {\bf b}_{ik}))
      S({\bf V})
    \bigg],
\end{align*}
which through equation (\ref{eq:if-id}) yields the influence function
\begin{align*}
          U_{1} =
    \sum_i\bigg(
          \frac{\exp(\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}))}{Z}
          \times 
  \sum_k \frac{\mathbb{I}[{\bf b}_{ik}' = {\bf b}_{ik}, {\bf a}_{ik}' = {\bf a}_{ik}]}{P({\bf b}_{ij})}\bigg)  -
     \psi
\end{align*}
for the first term of (\ref{eq:bounds-pathwise-derivative}). A nearly identical
argument can be used to derive the contributions of the second and third terms,
which are
\begin{align*}
  U_{2} = \sum_i \bigg(
    \frac{\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik})
    \exp(\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}))}{Z}
    \times \sum_k \big(\frac{\mathbb{I}[{\bf b}_{ik}' = {\bf b}_{ik}, {\bf a}_{ik}' = {\bf a}_{ik}]}{P({\bf b}_{ij})} -
     P({\bf a}_{ik} \mid {\bf b}_{ik})\big)\bigg)
          \end{align*}
          \begin{align*}
  U_{3} = \sum_i
  \frac{\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik})
  \exp(\alpha_0 \sum_{k} P({\bf a}_{ik} \mid {\bf b}_{ik}))}{Z}  U_1.
\end{align*}
This yields the influence function $U(\psi(\alpha_0)) = U_1 + U_2 + U_3$, when
functionals in the maximand set are sums of marginal or conditional
distributions of the observed variables. The derivation becomes more complicated
when functionals in the maximand have more complex forms, and we defer
consideration of such cases.

\subsubsection*{Status}

We are interested in extending this approach to estimands with more complex
identifying functionals. In addition, we plan to study more involved scenarios,
in which the causal model places equality constraints on the observed data
distribution and the influence functions presented may not be efficient.

\end{document}
