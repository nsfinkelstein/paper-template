\documentclass[main.tex]{subfiles}

\begin{document}

\section{Logic-Based Bounds on Causal Targets}
\label{sec:causal-bounds}

Up to this point, we have only considered bounds that can be obtained by solving
linear programs. In this section, we develop methods for deriving bounds that do
not depend on this property. We begin by introducing a number of simple bounds
that will serve as building blocks in bounding counterfactual parameters, in
that they will be used together in various combinations. The first of these
is a simple fact of basic probability theory, which we refer to as the
entailment bounds.

\begin{remark}[Entailment Bounds]
  $(A \implies B) \implies P(A) \le P(B)$.
\end{remark}

\noindent These follow simply from the fact that $A \implies B$ can be
interpreted to mean that $A$ is a subset of $B$, and subsets must have smaller
measure than their supersets. Next, we consider the Fr\'echet bounds.

\begin{remark}[Fr\'echet Bounds]
  $\max \{0, P(A) + P(B) - 1 \}
  \le P(A, B)
  \le \min \{P(A), P(B)\}$
\end{remark}

\noindent The lower of these bounds simply takes advantage of the
fact that the whole outcome space has measure $1$, such that two events with
large probability must overlap, and the upper bound simply states that they
cannot overlap on an area greater than the smallest between them. Though these
bounds are very simple, they are sharp when we have no information about the
relationship between the two events.

We now introduce the consistency bounds, the first to involve reference to
distributions of counterfactual random variables. Recall that causal consistency
states that $Y = y \land A = a$ implies $Y(A = a) = y$.
\begin{remark}[Consistency Bounds]
  $P(Y(A = a) = y) \in \left[
    P(A = a, Y = y),
    1 - \sum_{\tilde y \neq y} P(A = a, Y = \tilde y)
  \right],$
\end{remark}
\noindent The lower bound follows immediately from the entailment bounds, and
the upper bound is a consequence of the fact that $\sum_y P(Y(A = a) = y) = 1$;
if $P(Y(A = a) = y)$ exceeds the stated upper bound, $P(Y(A = a) = \tilde
y)$ for some $\tilde y \neq y$ would have to subceed its lower bound for $P(Y(A
= a))$ to sum to $1$.

Next we introduce the exclusion bounds. A variable $Z$ is said to have an
exclusion restriction from a variable $Y$ given another variable $A$ if the
effect of $Z$ on $Y$ is \textit{fully mediated} by $A$. Graphically, this
corresponds to all directed paths between $Z$ and $Y$ going through $A$.
Mathematically, it can be expressed as $Y(A = a, Z = z) = Y(A = a)$ because,
after intervention on $A$, further intervention on $Z$ will have no effect on
$Y$.

\begin{remark}[Exclusion Bounds]
  If there exists an exclusion restriction between $Z$ and $Y$ given $A$,
  \begin{align}
  P(Y(a) = y) \in \left[ \max_z P(A(z) = a, Y(z) = y), 1 - \sum_{\tilde y \neq y} \max_z P(A(z) = a, Y(z) = \tilde y)  \right].
  \end{align}
\end{remark}

\noindent By consistency, $Y(Z = z) = y \land A(Z = z) = a$ implies $Y(A = a, Z
= z)$. By the exclusion restriction, $Y(A = a, Z = z) = Y(A = a)$. Combining
these facts directly yields that $Y(Z = z) = y \land A(Z = z) = a$ implies $Y(A
= a) = y$, which yields the exclusion bounds by way of the entailment bounds. If
$P(A(z) = a, Y(z) = y)$ is identified, these bounds can be obtained in terms of
the observed data distribution.

\subsubsection*{Bounds in Models with Mediators}

\begin{figure}[h!]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.0cm]
    \begin{scope}
      \tikzstyle{vertex} = [ draw, thick, ellipse, minimum size=4.0mm,
      inner sep=1pt ]

      \tikzstyle{edge} = [ ->, blue, very thick ]


      \node[vertex, circle] (a) {$A$};
      \node[vertex, circle] (m) [right of=a] {$M$};
      \node[vertex, circle] (y) [right of=m] {$Y$};

      \draw[edge] (a) to (m);
      \draw[edge] (m) to (y);
      \draw[edge, red, <->] (a) [bend left=30] to (y);
      \draw[edge, red, <->] (m) [bend right=30] to (y);
    \end{scope}
  \end{tikzpicture}
  \caption{ The Confounded Frontdoor Model }
  \label{fig:confounded-frontdoor}
\end{figure}


We now demonstrate how these bounds can be used to obtain bounds on $P(Y(a))$
in causal graphical model depicted in Figure \ref{fig:confounded-frontdoor},
which we refer to as the confounded frontdoor model. This model describes
settings in which treatment is fully mediated through an observed mediator, and
both treatment and the mediator are confounded with outcome. It may be applied
in some medical settings. For example, in certain immunotherapy treatments the
effect of treatment on outcome may plausibly be thought to be fully mediated
through its effect on observable properties of the immune system and observable
side effects. The state of the immune system is likely to be correlated with
outcome for any number of reasons.

First, we note the following equalities
%
\begin{align}
  P(Y(a) = y) &= \sum_m P(Y(a) = y, M(a) = m)\\
  \label{eq:marginal}
  &= \sum_m P(Y(m) = y, M(a) = m),
\end{align}
%
\noindent where the second equality follows from consistency and the exclusion
restriction between $A$ and $Y$ given $M$. If we can bound
$P(Y(m) = y, M(a) = m)$, then, we can bound the target. According to the
Fr\'echet bounds,
%
\begin{align}
  \label{eq:unidentified-frechet}
  \max \{0, P(Y(m) = y) + P(M(a) = m) - 1 \}
  \le P(Y(m) = y, M(a) = m)
  \le \min \{P(Y(m) = y), P(M(a) = m)\}.
\end{align}
%
These bounds unfortunately cannot be evaluated, as $P(Y(m))$ is not identified
under the model. It can however be bounded according to the consistency bounds.
When the consistency bounds on $P(Y(m))$ are appropriately substituted into
(\ref{eq:unidentified-frechet}), we obtain
%
\begin{align*}
  \max \{0, P(m \mid a) + P(m, y) - 1\} \le
  P(Y(m) = y, M(a) = m)
  \le 
  \min \{1 - \sum_{y' \neq y} P(m, y'), P(m \mid a )\}.
\end{align*}
%
These bounds on $P(Y(m) = y, M(a) = m)$ are given in terms of the observed data
distribution, and can be used to bound $P(Y(a) = y)$ through Equation
(\ref{eq:marginal}). In addition, $P(Y(a) = y)$ can be bounded using the
consistency bounds directly. Combining these ideas, we obtain the ``mediator
bounds''. Let
%
\begin{align*}
      \label{eq:lower-mediator}
     &\xi_\ell(y(a)) \equiv 
       \max \bigg\{
       P(a, y), \sum_m \max\big\{0, P(m \mid a) + P(m, y) - 1
       \big\}\bigg\}\\
    \label{eq:upper-mediator}
     &\xi_u(y(a)) \equiv 
       \min \bigg\{1 - \sum_{y' \neq y}P(a, y),
       \sum_m \min
       \big\{1 - \sum_{y' \neq y} P(m, y'), P(m \mid a ) \big\} \bigg\}.
\end{align*}
Then the mediator bounds can be expressed as 
\begin{align*}
    \label{eq:mediator-bounds}
    P(y(a)) \in \left[
    \max\big\{ \xi_\ell(y(a)), 1 - \sum_{y' \neq y} \xi_u(y'(a))\big\},
    \min\big\{ \xi_u(y(a)), 1 - \sum_{y' \neq y} \xi_\ell(y'(a))\big\}
    \right].
\end{align*}

In the binary case, for example, the mediator bounds are
\begin{align}
  \label{eq:mediator-bounds}
  P(Y(a) = y) \ge \max
  \begin{cases}  
    P(a, y)\\
    P(m \mid a) + P(m, y) - 1\\
    P(\bar m \mid a) + P(\bar m, y) - 1\\
    P(\bar m, \bar y) - P(m \mid a)\\
    P(m, \bar y) - P(\bar m \mid a).
\end{cases}
\end{align}

We believe that it will be possible to obtain similar bounds in additional
scenarios in which the effect of treatment on outcome is not identified, but is
fully mediated through observable variables, and plan to develop a procedure for
obtaining such bounds as part of the proposed thesis.

\subsubsection*{Bounds in Models with Instruments}

In this section, we describe how the instrumental variable
bounds, derived in \cite{balke1997thesis} through linear programming, can be
derived without making use of the linearity of the model. We then present a
procedure for applying the same kind of reasoning to any model involving
instruements, whether it can be linearly expressed or not. The instrumental
variable bounds in the binary case are
\begin{align}
  \label{eq:iv-bounds}
  P(Y(a) = y) \ge \max \begin{cases}
      P(a, y \mid z)\\
      P(a, y \mid \bar z)\\
      P(a, y \mid z) + P(\bar a, \bar y \mid z)
      - P(\bar a, \bar y \mid \bar z) - P(a, \bar y \mid \bar z)\\
      P(a, y \mid z) + P(\bar a, y \mid z)
      - P(\bar a, y \mid \bar z) - P(\bar a, \bar y \mid \bar z).
    \end{cases}
  \end{cases}
\end{align}
The first two bounds in the $\max$ follow directly from the exclusion bounds,
described above. To reason about the remaining bounds, we begin by noting that
the exclusion restriction tells us that some events in the hypothetical worlds
in which $Z$ is set to $z$ and $\bar z$ are \textit{incompatible} with each
other, i.e. that no single individual can experience both of. For example, a
subject who experiences $A(z) = \bar a, Y(z) = y$ cannot experience $A(\bar z) =
\bar a, Y(\bar z) = \bar y$, as this would imply that intervention on $Z$
affects $Y$ other than through $A$, in violation of the model.

Consider each of the four outcomes in the event space under intervention on
$Z$. First, $A(z) = a, Y(z) = y$ implies $Y(a) = y$, from which we obtain the
exclusion bounds. Next, $A(z) = a, Y(z) = \bar y$ is \textit{incompatible} with
$Y(a) = y$; it is impossible to experience both in their respective
hypothetical worlds. Finally, $A(z) = \bar a, Y(z) = y$ and $A(z) = \bar a, Y(z)
= \bar y$ are \textit{compatible} with $Y(a) = y$, but do not imply it.

We would like to find out which portion of the population that experiences each
of these latter two events also experiences $Y(a) = y$. Unfortunately due to
confounding between $A$ and $Y$, we do not have direct access to the
hypothetical world induced by intervention on $A$. One way to get at this
number instead is by asking: what portion of the population that experiences
these two events under intervention $Z = z$ also experiences $Y(\bar z) = y,
A(\bar z) = a$ under intervention $Z = \bar z$, and therefore necessarily $Y(a)
= y$ under intervention $A = a$?

We first consider the event $A(z) = \bar a, Y(z) = y$. As described above, this
event is incompatible with $A(\bar z) = \bar a, Y(\bar z) = \bar y$. Therefore
every subject who experiences the former under $Z = z$ experiences one of the
three \textit{other} events under $Z = \bar z$. To learn what portion of these
\textit{must} also experience $A(\bar z) = a, Y(\bar z) = y$, we can simply
subtract out the measure of the remaining two compatible events, $A(\bar z) =
\bar a, Y(\bar z) = y$ and $A(\bar z) = a, Y(\bar z) = \bar y$. This yields the
following bound:
%
\begin{align*}
  P(A(z) = \bar a, Y(z) = y, A(\bar z) = a, Y(\bar z) = y) \ge
  P(A(z) = \bar a, &Y(z) = \bar y) - P(A(\bar z) = \bar a, Y(\bar z) = y)\\
  &- P(A(\bar z) = a, Y(\bar z) = \bar y).
\end{align*}
%
Because $A(z) = \bar a, Y(z) = y, A(\bar z) = a, Y(\bar z) = y$ implies $Y(a) =
y$, and is disjoint from $A(z) = a, Y(z) = y$ (by virtue of $A(z) = \bar a$ in
the event), we can lower bound $Y(a) = y$ by adding the lower bound for this
event to $P(A(z) = a, Y(z) = y)$, which is equal to the third bound in the
$\max$ in the IV bounds. An identical line of reasoning will yield the final IV
bound.

This argument relies only on the fact that there is an exclusion restriction
between the instrument(s) $Z$ and the outcome(s) $Y$ given treatment(s) $A$, and
on the fact that the joint distribution of outcome(s) and treatment(s) after
intervention on instrument(s) is identified. A procedure based on these
principles can therefore be applied to any models in which these properties
hold, regardless of whether modeling constraints can be linearly expressed.

\begin{algorithm}[h]
	\caption{Lower Bounds on $P({\bf Y(a_1) = y})$}
		\label{alg:bounds}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} event $\bf Y(a_1) = y$
    \Statex \hspace{4mm} generalized instrument $\bf Z$
		\Statex \hspace{-6.5mm} \textbf{Output:} bounds on $P(\bf Y(a_1) = y)$
    \State Bounds = \{\}
    \State \textbf{For} $k  = 2, \dots, N$:
    \State \hspace{3mm} KBounds = \{\}
    \State \hspace{3mm} \textbf{For} $j = 1, \dots, M$:
    \State \hspace{6mm} \textbf{For} $E_1$ in $\{E_1 \implies \bf A(z_1) = a_k \}$
    \State \hspace{9mm} bound = $P(E_1) - P(\psi_{\bf z_j}(E_1) \land \neg \bf \big( A(z_j) = a_1 \land Y(z_j) = y \big))$
    \State \hspace{9mm} KBounds.add(bound)
    \State \hspace{6mm} \textbf{For} $E_2$ in $\{E_2 \implies \bf Y(z_j) = y \land A(z_j) = a_1 \}$
    \State \hspace{9mm} bound = $ P(E_2) - P(\psi_{\bf z_1}(E_2) \land \bf A(z_1) \neq a_k)$
    \State \hspace{9mm} KBounds.add(bound)
    \State \hspace{3mm} Bounds.add($\max$(KBounds))
    \State $\bf P(Y(z_1) = y_1, A(z_1) = a_1)$ + sum(Bounds)
	\end{algorithmic}
\end{algorithm}

This procedure is summarized in Algorithm \ref{alg:bounds}. In this procedure,
we use $\psi_{\bf z}(E)$ to indicate outcomes in the event space under
intevention $\bf Z = z$ that are \textit{compatible} with the event $E$, in the
sense described above. A proof of the correctness of this procedure, as well as
graphical criteria for determining event compatibility in less obvious
scenarios, is provided in \cite{finkelstein2020generalizediv}, as are a number
of applications of this procedure to derive bounds that cannot be obtained
through the linear programming method.

\begin{figure}[h!]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.5cm]
    \begin{scope}
      \tikzstyle{vertex} = [ draw, thick, ellipse, minimum size=7.0mm,
      inner sep=1pt ]

      \tikzstyle{edge} = [ ->, blue, very thick ]


      \node[vertex, circle] (z1) {$Z_1$};
      \node[vertex, circle] (a1) [right of=z1] {$A_1$};
      \node[vertex, circle] (y) [right of=a1] {$Y$};
      \node[vertex, circle] (a2) [right of=y] {$A_2$};
      \node[vertex, circle] (z2) [right of=a2] {$Z_2$};

      \draw[edge] (z1) to (a1);
      \draw[edge] (z2) to (a2);
      \draw[edge] (a1) to (y);
      \draw[edge] (a2) to (y);
      \draw[edge, red, <->] (a1) [bend left=30] to (y);
      \draw[edge, red, <->] (a2) [bend right=30] to (y);
      \draw[edge, red, <->] (a1) [bend right=30] to (a2);
    \end{scope}
  \end{tikzpicture}
  \caption{ The Double Instrumental Variable Model }
  \label{fig:double-iv}
\end{figure}

In this propsal, we examine a simple additional application. Suppose we are
interested in studying the effect of vaccination among roommates in a college
dorm. In this case, we collect variables $A_1$ and $A_2$ indicating whether each
student was vaccinated, and instruments $Z_1$ and $Z_2$, indicating whether each
student has a class in the building in which the college is administring free
vaccines, on the theory that this might encourage vaccination but will not
otherwise effect outcome. Finally, we have a single outcome variable $Y$, which
summarizes which roommates fall ill, and therefore has 4 possible values. We
suppose that treatment and outcome are confounded by each student's
inherent risk tolerance, and that $A_1$ and $A_2$ may be confounded by students
with similar risk tolerances tending to live together. This scenario is
represented by Figure \ref{fig:double-iv}.

The bounds obtained by the algorithm in this setting are depicted in Figure
\ref{fig:double-iv-bounds}. These bounds can be used to examine the effect of
policies such as vaccinating one roommate of each pair. We suggest that this
scenario is related to studies of causal inference under ``network effects'',
where one subject's outcome may be affected by others'. In this case, we allow
that the outcomes of each pair of roommates are related arbitrarily, and that
their treatments may be correlated. We plan to further investigate the
application of bounds in such settings where more restrictive assumptions about
the relationship between outcomes across units are plausible.

\begin{figure}
  \centering
  \include{double-iv-bounds}
  \caption{
    Bounds on $P(Y(A_1 = a_1, A_2 = a_2) = y_0)$ in the causal graphical model
    depicted by Figure \ref{fig:double-iv}. The syntax $P_{z,z}(\cdot)$ is used
    to indicate the distribution under intervention $Z_1 = z, Z_2 = z$.
  }
  \label{fig:double-iv-bounds}
\end{figure}

\subsubsection*{Status}

An elaboration of some of the ideas in this section were published in
\cite{finkelstein2020generalizediv}. Other ideas in this section are in
preparation for submission. We hope to develop a general algorithm
to bound causal targets for arbitrary causal graphical models using ideas from
this section and future work along similar lines.

\end{document}
