\documentclass[main.tex]{subfiles}

\begin{document}

\section{Preliminaries}
\label{sec:preliminaries}

We are interested in stochastic systems, summarized by a set of observed random
variables ${\bf V} \equiv V_1, \dots, V_N$. We make use of the
``manipulationist'' view of causality \cite{woodward2003making}, in which a
variable $A$ is said to be a cause of a variable $Y$ if, had $A$ been manipulatd
into taking a different value than the one it was observed to take naturally,
then $Y$ may also have taken a different value than it was observed to take.
This definition requires reasoning about hypothetical worlds in which different
manipulations have taken place. To facilitate this kind of reasoning, we define
``counterfactual'' or ``potential outcome'' random variables to represent $\bf
V$ in these hypothetical worlds. These random variables are indexed by the
intervention corresponding to their hypothetical world, so that $Y(A = a)$
represents the random variable $Y$ in the hypothetical world in which $A$ is set
to $a$. A causal target is any functional of the distribution of counterfactual
random variables. For example, the Average Treatment Effect (ATE)  can be
expressed as $\mathbb{E}[Y(a)] - \mathbb{E}[Y(\bar a)]$, where $a$ and $\bar a$
represent the treatments being contrasted.

Graphically, we can represent such stochastic systems as a directed acyclic
graph (DAG) $\mathcal G$, where each node represents a variable and a directed
edge $A \rightarrow Y$ indicates that $A$ is a cause of $Y$. If an unobserved
variable is a common cause of multiple variables in $\bf V$, it must be included
in the graph. In such cases not all variables in the graph are observed,
resulting in a hidden variable DAG.

The manipulationist view of causality has been motivated by the idea that each
random variable in $\bf V$ is a deterministic function of its causes as well as
some exogenous noise \cite{pearl2009causality}. If we let $pa_{\mathcal G}(V)$
represent the parents of $V$ in $\mathcal G$, and $\epsilon_{V}$ represent
exogenous noise relevant to determining the value of $V$, then under this view
there exists a structural equation $f_V$ -- generally unknown to us -- such that
$V = f_V(pa_{\mathcal G}(V), \epsilon_V)$. Because these equations are not
restricted in any way, this model is known as a nonparametric structural
equations model (NPSEM) \cite{richardson2013single}.

One important property of the NPSEM model is known as ``causal consistency''
\cite{malinsky2019pocalc}. This property forms a crucial bridge connecting the
observed world to the hypothetical worlds we seek to reason about by describing
conditions under which the observed values of $\bf V$ are equal to the values
taken by random variables in the relevant hypothetical worlds. Formally, causal
consistency states that $A = a \land Y = y \implies Y(a) = y$. Verbally, it says
that the observed outcome is the same as the outcome would have been, had the
treatment been exogenously set to its observed value rather than taking it
naturally. This property may not hold in fact if there are unmodeled causal
relationships between different units in the data
\cite{rosenbaum2007interference}.

In some cases, the constraints placed on the target of inference by the model
and the observed data can be expressed linearly in the parameters of the
distribution being modeled. If the target itself is a linear functional of that
distribution, it can be bounded -- minimized and maximized over distributions in
the model -- by solving a linear program (LP) \cite{bertsimas2007linear}. Such
programs have the canonical (primal) form

\begin{align}
  \label{eq:lp}
\text{optimize } c^Tx \text{~~~~~~~subject to }   Ax \le b,
\end{align}

\noindent where $x$ is a length $n$ vector of variables, $c$ is length $n$, $b$
is length $m$ and $A$ is a $m \times n$ matrix. The constraints $Ax \le b$
restrict $x$ to take values inside a convex polytope \cite{bertsimas2007linear}, and
the linearity of the objective entails that it is optimized at a vertex of the
polytope. We refer to this polytope as the ``primal polytope''. The
problem posed in (\ref{eq:lp}) can be equivalently expressed in its dual form

\begin{align}
  \label{eq:dual-lp}
  \text{optimize } b^Ty \text{~~~~~~~subject to }   Ay = c \text{ and } y \ge 0.
\end{align}

One popular approach to solving linear programs is the simplex algorithm
\cite{dantzig1956simplex}. We describe this algorithm in more detail in Section
\S \ref{sec:parametric-simplex}.

\end{document}
