\documentclass[main.tex]{subfiles}

\begin{document}

\section{Paritial Identification Under Measurement Error}
\label{sec:measurement-error}

In this section, we provide an approach to bounding functionals of the
factual and counterfactual distributions of a mismeasured variable when the
observed variables have finite cardinality. Measurement error occurs whenever
the true underlying phenomenon of interest is measured inexactly. In such cases,
analysts must make assumptions about the relationship of the variable of
interest to its observed proxy or proxies. In much of the existing literature,
these assumptions are strong and parametric, and yield exact identification of
the target \cite{carroll2006measurement, kuroki2014measurement}. In many cases,
such assumptions are not warranted.

Our approach to partial identification is to express modeling assumptions as
linear constraints on a joint distribution. We show that many common
measurement error assumptions can be expressed linearly. When models are linear,
sharp bounds can be obtained by solving a linear program, which we show how to
construct. When models are nonlinear, outer bounds on the target may be obtained
by assuming a linear supermodel, and solving the corresponding linear program.

Existing partial-identification results are often derived for specific
scenarios \cite{imai2010treatment}, or require a ``guess-and-check'' approach,
wherein analysts must solve a constrained optimization problem to check whether
any given value of the target is feasible \cite{molinari2008misclassified,
  henry2014partial}. We hope that our contribution increases the number of
applications in which partial identification results under plausible
assumption are easy to obtain.

\subsubsection*{Single-Proxy Scenario}

We begin by considering the simplest possible case of a single mismeasured
variable $X$ with a single observed proxy $Y$. For convenience, we let
$\phi_{xy} \equiv P(X = x, Y = y)$ denote the parameters of their joint
distribution, and suppose that $X$ and $Y$ share the same domain. We now provide
examples of measurement error assumptions that can be expressed as linear
constraints on these parameters:

\begin{enumerate}
  \item[(A0)] Bounded error proportion:
    $\sum_{x \neq y} \phi_{xy} \leq \epsilon$
  \item[(A1)] Unidirectional errors:
    $\sum_{y < x} \phi_{xy} \leq \delta$
  \item[(A2)] Symmetric error probabilities:
    $\phi_{xy} = \phi_{xy}\,\,\,\forall\,\,|x - y| = |x - y'|$
  \item[(A3)] Error probabilities decrease by distance:
    $\phi_{xy} \geq \phi_{xy'}\,\,\,\forall\,\,|x - y| > |x - y'|$
\end{enumerate}

(A0) states that the total proportion of the population that have been
mismeasured is bounded from above by $\epsilon$. (A1) states that all errors are
generally in the same direction. This assumption is commonly made in scenarios
such as self-reporting of bad behaviors, where it is often assumed that almost
everyone will under-report the behavior. (A2) states that errors in either
direction are equally likely, which is common when errors are due to physical
processes. (A3) states that the proxy $Y$ taking a value closer to the true
variable $X$ is always more likely than it taking a farther value.

These assumptions, and many other common linear assumptions, can be manipulated
by analysts as appropriate. In addition, the assumptions can be easily altered
and mixed in different configurations to test the sensitivity of inference to
specific measurement-error models.

The joint distribution $\phi$ is also constrained by the non-negative
requirement $\phi_{xy} \ge 0 ~~\forall x, y$, and by the fact that the marginal
distribution it implies for the observed proxy $Y$ must match its observed
distribution, i.e. $\sum_x \phi_{xy} = P(Y = y)~~ \forall y$. These additional
constraints are also linear. It follows that all constraints on the joint
distribution represented by $\phi$ are linear.

Suppose we are interested in the expected value of the unobserved variable $X$.
This can be expressed linearly in terms of $\phi$ as $\mathbb{E}[X] = \sum_{x,
  y} x \phi_{xy}$. Our task, then, is to learn the maximum and minimum values
this linear functional of $\phi$ can take, such that $\phi$ lives in its
permitted convex polytope. This can of course be accomplished by solving a
linear program from the maximum and minimum values of the target.

Below, we provide an example of such a linear program. In this program, we
assume a variant of (A0), which posits that the proportion of the population for
whom the variable of interest is mismeasured by more than $2$ is bounded from
above by $\epsilon$. We also assume (A2) and (A3) as stated above, and seek to
bound $\mathbb{E}[X]$. The resulting linear program is:

\begin{align}
\label{eq:base}
&\text{objective:~~} &\sum_{x,y} & x \phi_{xy}\\\nonumber
&\text{constraints:~~} &\sum_x \phi_{xy} & = P(Y = y) \\\nonumber
& & \phi_{xy} & \ge 0 \\\nonumber
& & \sum_{|x - y| > 2} \phi_{xy} &\le \epsilon \\\nonumber
& & \phi_{xy} &\geq \phi_{xy'} & &
                    \forall |x - y | < |x - y'| \\\nonumber
& & \phi_{xy} &= \phi_{xy'} & &
                     \forall |x - y| = |x - y'|
\end{align}

The value of $P(Y)$ varies by dataset. This means that either the linear program
need be solved numerically for each observed data distribution, or it
can be solved symbolically in terms of $P(Y)$. The latter possibility is
discussed in detail in Section \S \ref{sec:parametric-simplex}.

\subsubsection*{Causal Bounds in the IV Scenario with measurement error}

\begin{figure}[h!]
  \centering
\begin{tikzpicture}[>=stealth, node distance=1.0cm]
  \begin{scope}
    \tikzstyle{vertex} = [
      draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt
    ]

    \tikzstyle{edge} = [
      ->, blue, very thick
    ]

    \node[vertex, circle] (z) {$Z$};
    \node[vertex, circle] (a) [right of=z] {$A$};
    \node[vertex, circle, gray] (x) [right of=a] {$X$};
    \node[vertex, circle] (y) [right of=x] {$Y$};
    \node[vertex, circle, red, opacity=.7] (u) [above of=x] {$U$};

    \draw[edge] (z) to (a);
    \draw[edge] (a) to (x);
    \draw[edge] (x) to (y);
    \draw[edge, red, opacity=.7] (u) to (a);
    \draw[edge, red, opacity=.7] (u) to (x);
    \draw[edge, red, opacity=.7] (u) to (y);
  \end{scope} 
\end{tikzpicture}
\caption{
  The instrumental variable scenario with measurement error on outcome. The
  observed outcome $Y$ is a noisy version of the unobserved true variable of
  interest $X$.
}
\label{fig:iv-me}
\end{figure}

We now consider bounds on counterfactual parameters. We use the important
example of the instrumental variable scenario with measurement error on outcome,
depicted in Figure \ref{fig:iv-me}. This scenario is common in IV applications
and the social sciences, but the fact of measurement error is often ignored in
the analysis. One simple example arises in studies on the question of the effect
of retirement age on health outcome. In \cite{coe2008age-iv}, for example, the
country-wide retirment age is used as an instrument for actual retirement age.
These are both recorded without error. However, health outcomes in the study are
self-reported, and may be erroneous for many reasons, including self-perception
bias, missed diagnoses, etc. In such settings, we advocate treating the observed
outcome as a proxy for the true variable of interest, and making reasonable
measurement error assumptions as described above.

In this case, we define a joint distribution over all counterfactual random
variables. Assuming all variables are binary valued, for example, these
counterfactual random variables are
$A(z), A(\bar z), X(a), X(\bar a), Y(x), \text{ and } Y(\bar x)$. For convenience
we denote the parameters of the joint distribution over these variables as
\begin{align}
\phi_{a_0a_1x_0x_1y_0y_1} \equiv P(
  A(z) = a_0, A(\bar z) = a_1,
  X(a) = x_0, X(\bar a) = x_1,
  Y(x) = y_0, Y(\bar x) = y_1
  ).
\end{align}
Suppose we did, in fact, observe $X$. Then by consistency, we would have the
following equality constraints by consistency and the randomization of the
instrument $Z$:
\begin{align}
  \label{eq:iv-constraint}
  P(A = a, X = x, Y = y \mid Z = z) = P(A(z) = a, X(a) = x, Y(x) = y).
\end{align}
The right hand side can easily be expressed linearly in terms of $\phi$,
as all the remaining random variables in the joint are simply marginalized out
of the distribution.

\begin{align}
  \label{eq:iv-constraint-psi}
  P(A = a, X = x, Y = y \mid Z = z) = \sum_{a_1,x_1,y_1} \phi_{aa_1xx_1yy_1}
\end{align}

Because we do not observe $X$, we cannot directly use Equation
(\ref{eq:iv-constraint-psi}) as a constraint on $\phi$. However, we can add
together the constraints corresponding to the same values of $A, Y$ and $Z$ to
obtain a new equality constraint in which $X$ is effectively marginalized out of
the conditional distribution $P(A, X, Y \mid Z)$. In the binary case, this
yields the constraint

\begin{align}
  \label{eq:iv-constraint-me}
  P(A = a, Y = y \mid Z = z) = \sum_{a_1,x_1,y_1} \phi_{aa_1xx_1yy_1} + \sum_{a_1,x_1,y_0} \phi_{aa_1\bar xx_1y_0y}.
\end{align}

It is easy to see that any moment of $X(a)$, the counterfactual unobserved
outcome, is linear in $\phi$. In some cases, it is possible to substantively
bound $X(a)$, and therefore the ATE, without additional assumptions. However
there may be good reason to assume treatment has a monotonic effect on outcome,
which can be expressed as a linear constraint on $\phi$ as follows
%
\begin{enumerate}
  \item[(A3)] Positive Effect of Treatment on Truth:
    $P(X(a) = x, X(a') = x') = 0\,\, \forall a' > a, x' < x$.
\end{enumerate}
%
Many other causal assumptions can be expressed linearly in $\phi$ as well. In
addition, the measurement error assumptions described above regarding the
frequency and size of errors can now be stated as constraints on the marginal
distributions $P(Y(x))$ and $P(Y(\bar x))$.

\subsubsection*{Status of this work}

A paper that presents and generalizes the ideas developed in this section to a
wider class of models that can be represented linearly is currently under
review. We plan to explore partial identification results in non-linear measure
error scenarios using ideas developed in later sections of this proposal.

\end{document}
