\documentclass[10pt]{beamer}

\usetheme{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage{setspace}
\doublespacing
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Methods for Partial Identification of Counterfactual Parameters}
\subtitle{GBO Thesis Proposal}
\date{12.01.2020}
\author{Noam Finkelstein}
\institute{Johns Hopkins University}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

\begin{frame}{Motivation}
  \begin{itemize}
    \item Memorial Sloan Kettering Cancer Center in NYC treats tens of thousands
      of patients each year. 
    \item But when it comes to making treatment decisions, they rely on clinical trials
    of tens or hundreds of patients.
  \end{itemize}
\end{frame}

\begin{frame}{Problems with using only clinical trials}

  \begin{itemize}
  \item Trials are very small -- statistical uncertainty is non-trivial, not all
    types of patients are represented.
  \item Trials intentionally select for healthier patients through restrictive
    eligibility criteria, and are run primarily at teaching hospitals in urban
    centers. 
  \item If treatment options are not directly compared in a trial, treatment
    decisions are often made on the basis of anecdotal evidence or left up to
    patients. 
  \end{itemize}
\end{frame}

\begin{frame}{Using observational data}

  \begin{itemize}
    \item Using historical data on hundreds of thousands of patients at MSKCC,
      or millions of patients nationwide, avoids these problems.

    \item But introduces new problems:
      
      \begin{itemize}
        \item \paragraph{\textbf{Unobserved Confounding:}} Patients who receive different
            treatments are sometimes different in important, unrecorded ways.
        \item \paragraph{\textbf{Measurement Error:}} Physicians often lie to insurance
            companies; patients sometimes lie to physicians; lab tests can be
            mistaken.
        \item \paragraph{\textbf{Missing Data:}} Patients often miss appointments,
          disappear from the historical record for unexplained reasons, etc.
      \end{itemize}

  \end{itemize}
    {\color{purple}\textbf{Motivating Question:}} To what extent can these
      problems be overcome, to enable use of observational data?
      

\end{frame}

\begin{frame}[fragile]{Setting}
    We collect data on factual variables ${\bf V} \equiv \{V_1, \ldots, V_N\}$.

    An edge $\bf A \rightarrow Y$ means $\bf A$ is a cause of $\bf
    Y$.

    For any given individual, if $\bf A$ had been exogenously set to
    a value different than observed, $\bf Y$ might have taken a different value
    as well.
    
    \begin{figure}
      \begin{center}
        \begin{tikzpicture}[>=stealth, node distance=1.0cm]
          \tikzstyle{format} = [draw, very thick, circle, minimum size=5.0mm,
          inner sep=0pt]

    \begin{scope}[yshift=0cm]
      \path[->, very thick] node[format] (a) {$A$} node[format, left of=a] (z)
      {$Z$} node[format, right of=a] (y) {$Y$} node[format, below of=a] (c)
      {$C$} node[format, above of=a, xshift=.5cm, opacity=.7] (u) {$U$}

      (z) edge[blue] (a) (a) edge[blue] (y) (c) edge[blue] (a) (c) edge[blue]
      (z) (c) edge[blue] (y) (u) edge[red, opacity=.7] (a) (u) edge[red,
      opacity=.7] (y) ;
    \end{scope}
  \end{tikzpicture}
\end{center}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Counterfactual Random Variables}

    \begin{figure}
      \begin{center}
        \begin{tikzpicture}[>=stealth, node distance=2.0cm]
          \tikzstyle{format} = [draw, very thick, circle, minimum size=8.0mm,
          inner sep=0pt]
          \begin{scope}[yshift=0cm]
            \path[->, very thick] node[format] (a) {\Large $A$} node[format,
            right of=a] (y) {\Large $Y$}

            (a) edge[blue] (y) ;
          \end{scope}
        \end{tikzpicture}
      \end{center}
    \end{figure}

    Imagine a world in which treatment $\bf A$ had been exogenously
    set to a value in its domain, $\bf a$. What would outcome $\bf Y$ have been?

    We denote this random variable $\bf Y(a)$, and call it a
    \textbf{counterfactual} random variable.
\end{frame}

\begin{frame}[fragile]{Counterfactual Parameters}

    Any functional of potential outcome distributions.

    \textbf{Average Treatment Effect (ATE):} $\psi = \mathbb{E}\left[ Y(a) - Y(\bar a) \right]$
\end{frame}

\begin{frame}[fragile]{Consistency}
\begin{figure}
  \centering \includegraphics[height=8cm]{consistency}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Point-Identification}
    A causal target $\psi$ is \textbf{point-identified} if,
    under the causal model, it is a function $\xi$ of the observed data
    distribution:
    \[\psi = \xi(P({\bf V})).\]
    Often not available in historical data.
\end{frame}

\begin{frame}[fragile]{Partial-Identification}
    A causal target $\psi$ is \textbf{partially-identified} if we can tell from
    $P({\bf V})$ and the causal model that $\psi$ must lie in some set (often an
    interval).
\end{frame}

\begin{frame}{Full v. Observed Data}
    \begin{figure}
      \begin{center}
        \begin{tikzpicture}[>=stealth, node distance=2.0cm]
          \tikzstyle{format} = [draw, very thick, circle, minimum size=8.0mm,
          inner sep=0pt]
          \begin{scope}[yshift=0cm]
            \path[->, very thick] node[format] (a) {\Large $A$} node[format,
            right of=a] (y) {\Large $Y$}

            (a) edge[blue] (y) ;
          \end{scope}
        \end{tikzpicture}
      \end{center}
    \end{figure}

  Full data includes $A$, $Y(a)$ and $Y(\bar a)$; observed data includes
  $Y$ and $A$.

  The full data law \textit{marginalizes} to the observed data law.
  \begin{align*}
    P(A = a, Y = y) = \sum_{y'} P(A = a, Y(a) = y, Y(\bar a) = y')
  \end{align*}
\end{frame}


\begin{frame}[fragile]{Linear Programs}
  \begin{itemize}
    \item Counterfactual models are constraints on the full data law.
    \item Counterfactual parameters are functionals of the full data law.
    \item If both can be expressed linearly, we obtain a linear program.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Linear Programs for Measurement Error / Causal Inference}

  \begin{itemize}
  \item Marginalization constraints are linear
  \item Many measurement error assumptions are linear:
  \begin{enumerate}
    \item[-] Frequency of measurement errors
    \item[-] Proportion of population with monotonic errors
    \item[-] Small errors are more common than large errors
    \item[-] etc.
  \end{enumerate}
  \item Many causal assumptions can also be expressed linearly:
  \begin{enumerate}
    \item[-] Exclusion restrictions
    \item[-] Proportion of population with monotonic effect
    \item[-] Larger doses of treatment lead to larger effects
    \item[-] etc.
  \end{enumerate}
\end{itemize}
\end{frame}


\begin{frame}[fragile]{Symbolic Solutions to Linear Programs}
  \begin{itemize}
  \item LP constraints involve the observed data law:
  \begin{align*}
    \hspace{-1.0cm}
    P(A = a, Y = y \mid Z = z)
    &= P(A(z) = a, Y(z) = y) \\
    &= \sum_{a', y'} P(A(z) = a, A(\bar z) = a', Y(a) = y, Y(\bar a) = y')
  \end{align*}
  \item Can be run numerically, or solved symbolically if target does not
    involve observed data law (through vertex enumeration).
  \vspace{0.5cm}
  \item What if target requires reference to observed data law?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Parametric Simplex Algorithm}
  Let $P({\bf V})$ represent the observed data law, and $\phi$ represent the
  parameters of the full data law.
  \begin{align*}
    \text{optimize~~} c(P({\bf V}))^T \phi \text{~~subject to } A\phi = b(P({\bf V})), ~~\phi \ge 0
  \end{align*}

  The parametric simplex keeps track of which values of $P({\bf V})$ correspond
  to different paths along vertices
\end{frame} 


\begin{frame}[fragile]{Importance of Symbolic Solutions}
  Why not just solve numerical procedures?

  \begin{itemize}
    \item Symbolic solutions may be easier to use.
    \item Symbolic solutions may offer insight into the structure of the
      problem.
    \item Estimation theory for results of numerical optimization procedures is
      not well developed.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Partial identification w/o linear programming}
  \begin{figure}
    \begin{center}
      \begin{tikzpicture}[>=stealth, node distance=2.0cm]
        \tikzstyle{format} = [draw, very thick, circle, minimum size=8.0mm,
        inner sep=0pt]
        \begin{scope}[yshift=0cm]
          \path[->, very thick] node[format, left of=a] (z) {\Large $Z$}
          node[format] (a) {\Large $A$} node[format, right of=a] (y) {\Large
            $Y$} node[format, above of=a, xshift=1cm, opacity=.5, yshift=-0.7cm]
          (u) {\Large $U$}

          (a) edge[blue] (y) (z) edge[blue] (a) (u) edge[red] (a) (u) edge[red]
          (y) ;
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{figure}
  \[
    \mathbb{E}\left[ Y(a) \right] \ge \max \begin{cases}
      P(a, y \mid z)\\
      P(a, y \mid \bar z)\\
      P(a, y \mid z) + P(\bar a, y \mid z)
      - P(\bar a, y \mid \bar z) - P(\bar a, \bar y \mid \bar z)\\
      P(a, y \mid z) + P(\bar a, \bar y \mid z) - P(a, \bar y \mid \bar z) -
      P(\bar a, \bar y \mid \bar z)
    \end{cases}
  \]
\end{frame}

\begin{frame}[fragile]{Exclusion Bounds for the IV Model}

  \begin{figure}
    \begin{center}
      \begin{tikzpicture}[>=stealth, node distance=2.0cm]
        \tikzstyle{format} = [draw, very thick, circle, minimum size=8.0mm,
        inner sep=0pt]
        \begin{scope}[yshift=0cm]
          \path[->, very thick] node[format, left of=a] (z) {\Large $Z$}
          node[format] (a) {\Large $A$} node[format, right of=a] (y) {\Large
            $Y$} node[format, above of=a, xshift=1cm, opacity=.5, yshift=-0.7cm]
          (u) {\Large $U$}

          (a) edge[blue] (y) (z) edge[blue] (a) (u) edge[red] (a) (u) edge[red]
          (y) ;
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{figure}
  \vspace{-0.3cm} {\large
    \begin{align*}
      Y(z) &= y \land A(z) = a \implies Y(a) = y\\
      Y(\bar z) &= y \land A(\bar z) = a \implies Y(a) = y\\
           &\\
      P(Y(a) = y) &\ge \max \begin{cases}
        P(Y = y, A = a \mid Z = z)\\
        P(Y = y, A = a \mid Z = \bar z)
      \end{cases}
    \end{align*}
  }
\end{frame}
% If you recall, there were a few more bounds in the Iv. How do we get those?

\begin{frame}[fragile]{Exclusion Bounds Intuition}
  \begin{figure}
    \centering \includegraphics[height=7cm]{single-world-bounds}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Cross-World Bounds Intuition}
  \begin{figure}
    \centering \includegraphics[height=7cm]{cross-world-bounds}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Event Space}
  \begin{figure}
    \centering \includegraphics[height=7cm]{event-space}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Compatible Events}
  \begin{figure}
    \centering \includegraphics[height=5cm]{event-space-highlighted}
  \end{figure} {\large What portion of the highlighted subpopulation has $Y = y,
    A = a$ in the \textit{other} hypothetical world (with $Z = \bar z$)? }
\end{frame}

\begin{frame}[fragile]{Cross-World Event Compatibility}
  \begin{figure}
    \centering \includegraphics[height=5cm]{cross-world-event-space}
  \end{figure}
  {\large Subtracting out the population compatible with $Y = y, A
    = \bar a$ that does not have $Y = y, A = a$ under $Z = \bar z$ yields }
  \vspace{-.5cm}
  \[P(Y = y, A = \bar a \mid Z = z) - P(Y = \bar y, A = a \mid Z = \bar z) - P(Y
    = y, A = \bar a \mid Z = \bar z)\]
\end{frame}

\begin{frame}[fragile]{Cross-World Bounds}

  {\large \centering Adding in the portion fo the population that has $Y = y, A
    = a$ under $Z = z$ yields:

  } \vspace{-.5cm}
  \begin{align*}
    P(Y = y, &A = a \mid Z = z) + P(Y = y, A = \bar a \mid Z = z)\\
             &- P(Y = \bar y, A = a \mid Z = \bar z) - P(Y = y, A = \bar a \mid Z = \bar z)
  \end{align*}
\end{frame}

\begin{frame}[fragile]{We've recovered the IV bounds}

  \begin{figure}
    \begin{center}
      \begin{tikzpicture}[>=stealth, node distance=2.0cm]
        \tikzstyle{format} = [draw, very thick, circle, minimum size=8.0mm,
        inner sep=0pt]
        \begin{scope}[yshift=0cm]
          \path[->, very thick] node[format, left of=a] (z) {\Large $Z$}
          node[format] (a) {\Large $A$} node[format, right of=a] (y) {\Large
            $Y$} node[format, above of=a, xshift=1cm, opacity=.5, yshift=-0.7cm]
          (u) {\Large $U$}

          (a) edge[blue] (y) (z) edge[blue] (a) (u) edge[red] (a) (u) edge[red]
          (y) ;
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{figure}
  \vspace{-0.3cm}
  \[
    \mathbb{E}\left[ Y(a) \right] \ge \max \begin{cases}
      \textbf{\color{violet}Exclusion Bounds:}\\
      \bf \color{violet}P(a, y \mid z)\\
      \bf \color{violet}P(a, y \mid \bar z)\\
      \textbf{\color{blue}Cross-World Bounds:}\\
      \bf \color{blue}P(a, y \mid z) + P(\bar a, y \mid z)
      - P(\bar a, y \mid \bar z) - P(\bar a, \bar y \mid \bar z)\\
      \bf \color{blue}P(a, y \mid z) + P(\bar a, \bar y \mid z) - P(a, \bar y
      \mid \bar z) - P(\bar a, \bar y \mid \bar z)
    \end{cases}
  \]
\end{frame}

\begin{frame}{Example: Mediated IV}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{figure}[t]
        \centering
        \begin{tikzpicture}[>=stealth, node distance=1.0cm]
          \begin{scope}
            \tikzstyle{vertex} = [ draw, thick, ellipse, minimum size=4.0mm,
            inner sep=1pt ]

            \tikzstyle{edge} = [ ->, blue, very thick ]


            \node[vertex, circle] (z) {$Z$}; \node[vertex, circle] (a) [right
            of=z] {$A$}; \node[vertex, circle] (m) [right of=a] {$M$};
            \node[vertex, circle] (y) [right of=m] {$Y$};

            \draw[edge] (z) to (a); \draw[edge] (a) to (m); \draw[edge] (m) to
            (y); \draw[edge, red, <->] (a) [bend right=30] to (y); \draw[edge,
            red, <->] (m) [bend left=30] to (y);
          \end{scope}
        \end{tikzpicture}
        \caption{ Mediated IV }
        \label{fig:graphs}
      \end{figure}
    \end{column}
    \begin{column}{0.6\textwidth}
  \begin{singlespace}
{\small\begin{align*}
         &P_{\bar a}(\bar y) \ge
P_{\bar z}(\bar a, \bar y) +
\\ &     \max \begin{cases} 0\\
            P_{z}(\bar a, \bar y, \bar m)
            - P_{\bar z}(\bar a, \bar m, \bar y)
\\
            P_{z}(\bar a, \bar y)
            - P_{\bar z}(\bar a, \bar y)
\\
            P_{\bar z}(a, \bar m, y)
            - \big(P_{z}(\bar m, y)
 + P_{z}(\bar a, m, y)
\big)\\
            P_{\bar z}(a, m, y)
            - \big(P_{z}(m, y)
 + P_{z}(\bar a, \bar m, y)
\big)\\
            P_{\bar z}(a, \bar y)
            - \big(P_{z}(a, \bar y)
 + P_{z}(\bar a, y)
\big)\\
            P_{\bar z}(a, m)
            - \big(P_{z}(a, m, \bar y)
 + P_{z}(m, y)
 + P_{z}(\bar a, \bar m, y)
\big)\\
            P_{z}(\bar a, \bar y, m)
            - P_{\bar z}(\bar a, m, \bar y)
\\
            P_{\bar z}(a, m, \bar y)
            - \big(P_{z}(\bar a, \bar m, y)
 + P_{z}(a, m, \bar y)
\big)\\
            P_{\bar z}(a, \bar m, \bar y)
            - \big(P_{z}(\bar a, m, y)
 + P_{z}(a, \bar m, \bar y)
\big)\\
            P_{\bar z}(a, y)
            - P_{z}(y)
\\
            P_{\bar z}(a, \bar m)
            - \big(P_{z}(\bar m, y)
 + P_{z}(\bar a, m, y)
 + P_{z}(a, \bar m, \bar y)
\big)
\end{cases}\end{align*}}
\end{singlespace}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Mediator Bounds in Binary Confounded Front-Door Scenario}

  \centering It's not necessary to have an instrument.
  
\begin{figure}
  \centering
\begin{tikzpicture}[>=stealth, node distance=1.3cm]
\tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
\tikzstyle{edge} = [->, blue, very thick]

	\begin{scope}
    \node[vertex, circle] (a) {$A$};
    \node[vertex, circle] (m) [right of=a] {$M$};
    \node[vertex, circle] (y) [right of=m] {$Y$};

    \draw[edge] (a) to (m);
    \draw[edge] (m) to (y);
    \draw[edge, red, <->] (a) [bend left=30] to (y);
    \draw[edge, red, <->] (m) [bend right=30] to (y);
  \end{scope}
\end{tikzpicture}
\end{figure}

\begin{singlespace}
\begin{align}
  \label{eq:mediator-bounds}
  P(Y(a) = y) \ge \max
  \begin{cases}  
    P(a, y)\\
    P(m \mid a) + P(m, y) - 1\\
    P(\bar m \mid a) + P(\bar m, y) - 1\\
    P(\bar m, \bar y) - P(m \mid a)\\
    P(m, \bar y) - P(\bar m \mid a).
\end{cases}
\end{align}
\end{singlespace}
\end{frame}

\begin{frame}[fragile]{Explanation}
  \begin{itemize}
    \item Suppose we observe a patient who experiences an unexpected outcome.
      How do we explain the poor outcome for that patient?
    \item Causal contrastive explanation: ``A = a explains Y = y if, had A not
      been a, Y would not have been y.''
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Explanation Scenario}
  \begin{itemize}
    \item A patient is enters MSKCC and is presented with three treatment options,
      $a_1, a_2, a_3$.
    \item Her oncologist does not know which is better for her - they decide
      treatment $a_1$ at random.
    \item Outcome is worse than expected. Is poor outcome explained by treatment $a_1$?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Probability of Explanation}
\begin{figure}
  \centering
\begin{tikzpicture}[>=stealth, node distance=1.3cm]
\tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
\tikzstyle{edge} = [->, blue, very thick]

	\begin{scope}
    \node[vertex, circle] (a) {$\bf C$};
    \node[vertex, circle] (m) [right of=a] {$A$};
    \node[vertex, circle] (y) [right of=m] {$Y$};

    \draw[edge] (a) to (m);
    \draw[edge] (m) to (y);
    \draw[edge] (a) [bend left=30] to (y);
  \end{scope}
\end{tikzpicture}
\end{figure}
  \begin{align*}
    \text{Probability of Explanation~} = \max_{a' \neq a_1} P(Y(a') \neq y \mid Y = y, A = a_1, {\bf C = c}),
  \end{align*}
Interpretation: The probability $Y = y$ is \textit{explained by} the fact that
$A = a_1$ instead of $A = a'$ in a patient for whom $A = a_1, Y = y, \bf C = c$.
\end{frame}

\begin{frame}[fragile]{Extended Explanation Scenario}
  \begin{itemize}
    \item Suppose patient selects $a_1$ for unknown reasons (e.g. patient is
      able to tolerate side-effects due to underlying health status, which will
      also affect outcome).
    \item Can still be non-trivially bounded by isolating cross-world event and
      applying Fr\'echet bounds, in the presence of bounds on $P(Y(a'))$.
    \item Probability of explanation (and other probabilities of causation) are
      no longer linear in the presence of other variables.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Bounds on (PE) in the Instrumental Variable Scenario}
  Consider the probability that $A = a$ explains $Y = y$ having also observed $Z
  = z$ in the binary instrumental scenario. Bounds are proportional to:

  {\footnotesize
  \begin{equation*}
    \hspace{-.6cm}
    \max \{0,
    \max
    \left\{\begin{align*}
      &P(\bar a, \bar y \mid z)\\
      &P(\bar a, \bar y \mid \bar z)\\
      &P(\bar a, \bar y \mid z) + P(a, \bar y \mid z)
      - P(a, \bar y \mid \bar z) - P(a, y \mid \bar z)\\
      &P(\bar a, \bar y \mid z) + P(a, y \mid z) - P(\bar a, y \mid \bar z)
      - P(a, y \mid \bar z)
        \end{align*}
      \right\}
  + P(Y = y, A = a \mid Z = z) - 1 \},
  \end{equation*}
  \begin{equation*}
    \hspace{-.6cm}
   \min \{1 - 
    \max
    \left\{\begin{align*}
      &P(a, y \mid z)\\
      &P(a, y \mid \bar z)\\
      &P(a, y \mid z) + P(\bar a, y \mid z)
      - P(\bar a, y \mid \bar z) - P(\bar a, \bar y \mid \bar z)\\
      &P(a, y \mid z) + P(\bar a, \bar y \mid z) - P(a, \bar y \mid \bar z) -
      P(\bar a, \bar y \mid \bar z)
    \end{align*}\right\},
       P(Y = y, A = a \mid {Z = z})\}.
  \end{equation*}
  }
\end{frame}

\begin{frame}[fragile]{What makes for a good explanation?}
  \begin{itemize}
    \item Interviews / surveys with clinicians, discussing simulated scenarios
      and possible explanations.
    \item Case studies with the algorithms to evaluate clinician response to the
      proposed explanations.
    \item How can background scientific knowledge be associated with the graph
      (e.g. literature backing the existence of each edge) to increase the
      ability of explanations to impart understanding?
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Missing Data}
  \begin{itemize}
    \item If data is selectively missing after it has been generated, we can ask
      about what we would have seen, had the data not been missing.
    \item We can use similar approaches, but with special care to ``selection
      bias'' -- incomplete information in hypothetical worlds after intervening
      on random variables with partially observed parents.
  \end{itemize}
\end{frame} 

\begin{frame}[fragile]{Estimation}
  \begin{itemize}
    \item Influence function based estimators often have good efficiency and
      robustness properties.
    \item However they are only defined for smooth functionals.
    \item As explored above, many partial-identification results involve involve
      maxima, which are not smooth.
    \item We plan to propose smooth relaxations of such functionals and derive
      influence functions for those relaxations
    \item Finally we will experimentally study the properties of the
      corresponding estimators, as well as the tradeoff between smoothness of
      the functional and sharpness of the corresponding bounds.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Conclusion}
  \begin{itemize}
    \item In realistic causal models, point-identification is often thwarted by
      confounding, missingness, and measurement error.
    \item Partial-identification tools are important (and understudied) 
      \begin{itemize}
        \item Allow for substantive causal inference despite obstacles.
      \end{itemize}
    \item The proposed thesis will seek to advance the partial identification
      literature in several important settings.
  \end{itemize}
\end{frame}

% especially if you have some data and want to derive bounds by these methods

%   \begin{frame}[allowframebreaks]{References}
%     \bibliography{bib} \bibliographystyle{abbrv}
%   \end{frame}

\end{document}
